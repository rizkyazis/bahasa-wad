<?php
session_start();
if (!isset($_SESSION['login'])) {
    echo "<script>alert('Login dulu bray');window.location.href='login.php'</script>";
}
include "route/connection.php";
$id = $_SESSION['id'];
$sql = mysqli_query($connect, "SELECT * FROM users WHERE id ='$id'");
if ((mysqli_num_rows($sql) > 0)) {
    while ($row = mysqli_fetch_array($sql)) {
        $email = $row['email'];
        $image = $row['img_path'];
        $nama = $row['nama'];
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <!-- NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="home.html">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Profile
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <center>
                            <a class="dropdown-item " href="profile.php">
                                <img src="upload/<?= $image ?>" style="width:70px;" class="bd-placeholder-img rounded-circle">

                                <br>
                                <b><?= $nama ?></b>
                            </a>

                            <hr>
                            <a class="dropdown-item" href="route/logout.php">Logout</a>
                        </center>

                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR  -->

    <div style="margin-top: 5%;">
        <div class="container">
            <!-- MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK -->

            <div>
                <center>
                    <h3>Kuis</h3>
                    <div class='card-deck' style='padding: 10px;width:80%;'>
                        <?php
                        $sql = mysqli_query($connect, "SELECT * FROM level");
                        while ($row = mysqli_fetch_array($sql)) {
                            $id = $row['id'];
                            $img = $row['img'];
                            $title = $row['title'];
                            $level = $row['level'];
                            $description = $row['description'];
                            echo "
                            <div class='card' >
                            <img src='$img' class='card-img-top' alt='...'>
                            <div class='card-body'>
                                <h5 class='card-title'>$level</h5>
                            </div>
                            <a href='kuisview.php?id=$id'><button type='button;' class=' card-footer-dark btn btn-dark '
                                style='width: 100%;'>GO</button></a>
                        </div>";
                        }
                        ?>
                    </div>

                </center>
            </div>

            <!--  MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK MATERI TEXT BOOK -->
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>