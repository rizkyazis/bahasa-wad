<?php
include "route/connection.php";
session_start();
if(!isset($_SESSION['login'])){
    echo "<script>alert('Login dulu bray');window.location.href='login.php'</script>";
}
$idx = [];
$i=1;
$skor=0;
$sql = mysqli_query($connect, "SELECT * FROM bank_soal WHERE level_id = '1'");
while($row = mysqli_fetch_array($sql)){
    if(isset($_POST["option$i"])){
    $idx[$i]=$_POST["option$i"];
    if($idx[$i]==($row['jawaban'])){
        $skor=$skor+1;
    }
}
    if($i==10){

    }else{
        $i++;
}
}
$skorakhir = ($skor/$i)*100;
if($skorakhir<40){
    $img = 'sad.gif';
}else if($skorakhir<70){
    $img = 'ok.gif';
}else{
    $img = 'clap.gif';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Required meta tags -->
        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Document</title> 
</head>
<body>
    <!-- NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="home.html">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Profile
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <center>
                            <a class="dropdown-item " href="profile.php">
                                    <img src="upload/<?= $_SESSION['image'] ?>" style="width:70px;" class="bd-placeholder-img rounded-circle">
                            
                                <br>
                                <b><?php echo $_SESSION['nama'] ?></b>
                            </a>

                            <hr>
                            <a class="dropdown-item" href="route/logout.php">Logout</a>
                        </center>

                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR  -->
    <center>
    <div class="card" style="width: 18rem;margin-top: 8%;">
        <img src="images/<?=$img?>" class="card-img-top" alt="...">
        <div class="card-body">
            <p class="card-text">Selamat skor anda : <b><?=number_format($skorakhir,2)?></b></p>
            <p class="card-text">Jumlah soal benar : <b><?=$skor?></b></p>
            <p class="card-text">Jumlah soal : <b><?=$i?></b></p>
        </div>
        <a href="kuis.php"><button class="btn-primary" style="border-radius:5px">Kembali</button></a>
        <br>
    </div>
    </center>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>
</html>
