<?php
include "route/connection.php";
session_start();
if (!isset($_SESSION['login'])) {
    echo "<script>alert('Login dulu bray');window.location.href='login.php'</script>";
}
$id = $_SESSION['id'];
$sql = mysqli_query($connect, "SELECT * FROM users WHERE id ='$id'");
if ((mysqli_num_rows($sql) > 0)) {
    while ($row = mysqli_fetch_array($sql)) {
        $email = $row['email'];
        $image = $row['img_path'];
        $nama = $row['nama'];
    }
}

$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf', 'doc', 'ppt'); // valid extensions
$path = 'upload/'; // upload directory
if (isset($_FILES['image'])) {
    $img = $_FILES['image']['name'];
    $tmp = $_FILES['image']['tmp_name'];
    $nama = $_POST['name'];
    // get uploaded file's extension
    $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
    // can upload same image using rand function
    $final_image = rand(1000, 1000000) . $img;
    // check's valid format
    if (in_array($ext, $valid_extensions)) {
        $path = $path . strtolower($final_image);
        if (move_uploaded_file($tmp, $path)) {
            $query = "UPDATE users SET img_path='$final_image', nama='$nama' where id='$id'";
            mysqli_query($connect, $query);
            //include database configuration file
            // include_once 'db.php';
            //insert form data in the database
            // $insert = $db->query("INSERT uploading (name,email,file_name) VALUES ('" . $name . "','" . $email . "','" . $path . "')");
            //echo $insert?'ok':'err';
        }
    } else {
        echo 'invalid';
    }
}
if (isset($_POST['name'])) {
    $name = $_POST['name'];
    $query = "UPDATE users SET nama='$name' where id='$id'";
    mysqli_query($connect, $query);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Edit Profile</title>
</head>

<body>
    <!-- NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="home.html">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Profile
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <center>
                            <a class="dropdown-item " href="profile.php">
                                <img src="upload/<?= $image ?>" style="width:70px;" class="bd-placeholder-img rounded-circle">

                                <br>
                                <b><?php echo $nama ?></b>
                            </a>

                            <hr>
                            <a class="dropdown-item" href="route/logout.php">Logout</a>
                        </center>

                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR NAVBAR  -->


    <div class="containter-fluid">
        <br>
        <br>
        <h1 class="h3 mb-4 text-gray-800 m-5">Edit Profile</h1>

        <div class="row">
            <div class="col-lg-8">

                <form id="profile" action="" method="" enctype="multipart/form-data" class="m-5">
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="<?= $email ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Full Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" placeholder="Full Name" name="name" value="<?= $nama ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">Picture</div>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="upload/<?= $image ?>" id="foto" class="img-thumbnail rounded-circle">
                                </div>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="image">
                                        <label class="custom-file-label" id="image">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row justify-content-end">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

    <script>
        $(document).ready(function(e) {
            $("#profile").on('submit', (function(e) {
                e.preventDefault();
                $.ajax({
                    url: "profile.php",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        //$("#preview").fadeOut();
                        // $("#err").fadeOut();
                    },
                    success: function(data) {
                        if (data == 'invalid') {
                            alert("Tipe file tidak diperbolehkan!");

                        } else {
                            alert("Berhasil update!");
                        }
                    },
                    error: function(e) {
                        alert("Error :(");
                        // $("#err").html(e).fadeIn();
                    }
                });
            }));

            // Function to preview image after validation
            $(function() {
                $(".custom-file-input").change(function(e) {
                    // $("#message").empty(); // To remove the previous error message
                    var file = e.target.files[0];
                    var imagefile = file.type;
                    var match = ["image/jpeg", "image/png", "image/jpg"];
                    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                        // $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                        return false;
                    } else {
                        var fileName = file.name;
                        $(this).next('.custom-file-label').html(fileName)

                        var reader = new FileReader();
                        reader.onload = function(e) {
                            // get loaded data and render thumbnail.
                            document.getElementById("foto").src = e.target.result;
                        };
                        // read the image file as a data URL.
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });
        });



        // $('.custom-file-input').on('change', function(e) {
        //     var fileName = e.target.files[0].name;
        //     $(this).next('.custom-file-label').html(fileName)

        //     var reader = new FileReader();
        //     reader.onload = function(e) {
        //         // get loaded data and render thumbnail.
        //         document.getElementById("foto").src = e.target.result;
        //     };
        //     // read the image file as a data URL.
        //     reader.readAsDataURL(this.files[0]);
        // })
    </script>
    <!-- <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>

</html>